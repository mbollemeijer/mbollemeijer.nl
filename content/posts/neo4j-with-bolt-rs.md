+++ 
draft = false 
date = 2022-08-04T10:38:31+02:00
title = "How to use bolt-rs for neo4j with deadpool-bolt"
description = ""
slug = ""
authors = ["mbollemeijer"]
tags = ["rust", "neo4j", "bolt", "bolt-rs", "deadpool", "deadpool-bolt"]
categories = ["dev"]
externalLink = ""
series = ["rust&neo4j"]
+++

# Background

In my day to day life I work with neo4j and I wanted to see how the support for the rust-lang was.   
While searching for crates from time to time I actually never tried one, until last week.

I settled for the bolt-rs collection that can be found here https://github.com/0xSiO/bolt-rs
On this repo you will find multiple crates all maintained by the same user "0xSiO" + maintainers.

The reason I am writing this post is because, the repo lacks ready to go examples (Only 1 and I didnt manage to get that one working)

# The example

The example provided resides in the "bolt-client" part of the repository https://github.com/0xSiO/bolt-rs/tree/master/bolt-client
That refers to https://docs.rs/bolt-client/latest/bolt_client/

Although I think the reason I did not get it to work has mostly todo with my experience in the rust-lang (pretty much none) and maybe with dependency mismatch.
One of the issue I was facing was that I could not get the correct "Stream" import working as in the example on the docu page.

# Connection pools

## bb8-bolt


I wanted to test this no matter what so I kept reading on the repo and took a look at the connection pools listed in the readme.
First I tried the bb8-bolt (cause who does not love bb8). But for the bb8-bolt crate no examples where given.

Tried and failed so I moved on, you can find the repo for bb8 here https://github.com/djc/bb8

## deadpool-bolt

Based on the crate downloads I went with https://github.com/bikeshedder/deadpool next.
But yet again no examples where given, but I deduced from the test cases provided in the deadpool-bolt crate how things should kinda work.

But the fact the I am not as strong in Rust as I would like did not make "the figuring out part" as easy as normal.

Below a simple code snippet on how to get deadpool-bolt to work.

First the deps

Config.toml
```toml
[dependencies]
tokio = { version = "1", features = ["full"] }
bolt-client = "0.10.1"
deadpool = "0.9.5"
deadpool-bolt = "0.1.0"

[dev-dependencies]
futures-util = { version = "0.3.0", default-features = false, features = ["alloc"] }
tokio = { version = "1.14.0", features = ["macros", "rt-multi-thread"] }
```

main.rs
```rust
use bolt_client::bolt_proto::version::{V3_0, V4_0, V4_1, V4_2};
use deadpool_bolt::{bolt_client::Metadata, Manager, Pool};

#[tokio::main]
async fn main() {

    let metadata = Metadata::from_iter(vec![
        ("user_agent", "my-client-name/1.0"),
        ("scheme", "basic"),
        ("principal", "<user>"),
        ("credentials", "<password>"),
    ]);

    let manager = Manager::new("127.0.0.1:7687", None, [V3_0, V4_0, V4_1, V4_2], metadata)
        .await
        .unwrap();

    println!("Connection setup");
    let pool: Pool = Pool::builder(manager).build().unwrap();

    let cloned_pool = pool.clone();
    let mut client = cloned_pool.get().await.unwrap();
    client.run("<YOUR CQL HERE>", None, None).await.unwrap();

    let (records, response) = client
        .pull(Some(Metadata::from_iter(vec![("n", 1)])))
        .await
        .unwrap();

    println!("{:?}", records);
    println!("{:?}", response);
}
```

This example should get you started!
