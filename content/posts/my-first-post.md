---
title: "My First Post... Again? Yes!"
date: 2018-04-25T09:43:13+02:00

# draft: true
---

# Again?
Yes again, for the 3rd of 4th time trying to start blogging about my professional and personal life as an developer. Well this time its going to be different, I think.. Maybe, Yes!

# Where do I start?
Lets start with my experience about setting up this blog and this awesome domain name, the idea about registering your lastname to a .codes TLD was picked up through a twitter user (would have given the credits if I remembered the user).
She had firstname.lastname.codes and I thought that was a pretty neat idea when you are blogging about code and tech related stuff. 

That being said, this inspired me to start blogging again or at least try to.

# Github, Gitlab or Bitbucket?
I don`t like the new design of the Atlassian products so bitbucket was quickly ruled out.
Lately I have been using gitlab over github and find that gitlab come with a lot of extra features that Github does not have, for me a big winner was the issue board a pretty basic version of what Atlassian offers with Jira.
I had some experience in my previous attempts to host Hugo pages on github and I found this to be allot of work before you had this working.  

So I was pleasantly surprised that Hugo keeps on growing and is integrated with a lot of other platforms since I last tried it, Gitlab included. And all these platforms that support build containers makes building ad publishing projects that much easier.    

# So... everything went smooth?
Well no, I wanted to use Gitlab for hosting my source and hugo pages with a custom (sub)domain name.
So I followed this guide [here](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) from the official documentation. So after following this guide I was able to see my pages on https://username.gitlab.io/projectName.
At first I was very pleased about how smooth the process went and thought to myself wow this is easy, one thing left. **the subdomain**.    

And here is where my troubles started, I had registered the .codes domain at [namecheap.com](https://namecheap.com) cause it was less then 10 euros apposed to my regular domain register [transip](https://transip.nl) where it was 55 euros. Normally I am very happy with **transip** but this time it was just too expensive considering **namecheap**`s price.

Having that domain and gitlab pages ready to go I followed the [official gitlab guide](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains) or at least try to.
I logged in to [namecheap.com](https://namecheap.com) to add an **CNAME** record to the domain **DNS** settings, what according to the guide should be this    
`subdomain.mydomain.com CNAME myusername.gitlab.io`   

Well name cheap does not allow you to make a **CNAME** record without a trailing dot, so when I would insert my config it would automatically add a trailing dot to my configuration.   
Like this `subdomain.mydomain.com CNAME myusername.gitlab.io.` **Gitlab** does not play nice with that trailing dot.

### So whats that dot then?
So I am gonna quote a rather simple explanation found on [serverfault](https://serverfault.com) what I think covers it.

> The . makes the name be relative to the root, without it, it the name will be relative to the current zone. The standard zone format is defined in rfc1035 and rfc1034.

Original post can be found [here](https://serverfault.com/a/18121/373026)

Because I could not remove the dot from the record, makes the lookup for my pages site on the gitlab part not possible because the root was not correct for it to resolve it.

### Now what?
So after hours struggling to get a workaround working by creating **A** records and several other **DNS** settings.
Gitlab was having issues, so to confirm, I tweeted. 

![Example image](/posts/my-first-post/tweet-to-gitlab.png)

I kept trying to get the workaround going because I had not get a response back yet, on that moment my CDI/Pipelines on gitlab were acting up and failing, so I already had noticed those issues on twitter when tweeting to Gitlab about my issue. After these Pipeline failures I was going to explore some other possibilities for hosting my site, cause I still had no response from **@Gitlabstatus** and I was determined to get it working, with or without gitlab.

### So its going to be Github or Bitbucket ?
I was already reading up on how the build process for hosting on [**Github**](https://github.com) would go, cause compared to **Gitlab** it`s a little more complicated.
On that note I stumbled on a post on [**Hugo**](https://gohugo.io) someone advising someone else to take a look at [**Netlify**](https://www.netlify.com/). Never heard of this platform so I had some reading to do.   

When I was up-to-speed I went back to google and found some [official docs](https://gohugo.io/hosting-and-deployment/hosting-on-netlify/) on how to use **Hugo** on **Netlify**.    
In short it had a few steps

1. Create an account on [Netlify](https://www.netlify.com/).
2. Authorize a webhook connection between **Gitlab/Github/Bitbucket** to Netlify.
3. Specify a build commando or include a netlify.toml for advanced configuration.

After completing these steps I got an auto generate url from **Netlify** and was able to preview my site, so that was easy.

### One more thing...
The custom domain, the one you are looking at in your browser.  So you got it working ? yhe, following the instruction listed [here](https://www.netlify.com/docs/custom-domains/).
I will summarize the difference in dns settings between **Gitlab pages** and **Netlify**

**Gitlab** wants you to add an **A** record for a root domain or a **CNAME** record for a subdomain (But without the trailing dot) to your **DNS** settings. 
Well, **Netlify** wants complete control and asks you to override the default **nameserver** settings from your DNS provider and sets it to those of **Netlify**.

I did registered this domain for the soul purpose of this website so I did just that, I changed the default **nameservers** to those of **Netlify**.
After that I create my subdomain in **Netlify** site settings and after all the servers were in sync my site was able to be reached through https://maikel.bollemeijer.codes and every time I push a new commit to **Gitlab** a webhook push is done to **Netlify** and they start building the new source, and when there are no issues while building, the new source gets deployed and your site is live.
I must say this a rather quick process, in less then 30 seconds after your push is finished **Netlify** has build your source and has deployed it.

# Mission complete!
Yes, the goal is reached but through **Netlify** connecting to **Gitlab** and with giving up the ability to use my domain for something else then this Blog.
For now this is a viable solution, but in the end I want control over my DNS records back, so I have to figure out how to keep control over my records but keep maintaining this connection with **Netlify**.   

Thanks for reading, let me know what you think or perhaps share your experience with setting up a static site.


